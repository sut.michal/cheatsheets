# MX Master 3 - Pairing in Ubuntu

1. Download Solaar
   ```bash
   $ sudo apt install solaar
   ```
2. Launch Solaar application and follow instructions to connect mouse. 
    * In case of problems, try unparing and pairing mouse again.


Taken from this thread:
https://askubuntu.com/questions/113984/is-logitechs-unifying-receiver-supported
