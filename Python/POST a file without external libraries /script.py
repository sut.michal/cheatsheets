import binascii
import os
from http import client
import mimetypes
from http.client import HTTPResponse
from typing import Tuple, TextIO


def file_to_body(field_name: str, file_pointer: TextIO) -> Tuple[bytes, str]:
    """
    Encode file into multipart/form-data body
    """
    boundary = binascii.hexlify(os.urandom(16)).decode('ascii')

    body = (
        "--{boundary}\r\n"
        "Content-Disposition: form-data; name=\"{field_name}\"; filename=\"{filename}\"\r\n"
        "Content-Type: {content_type}\r\n"
        "\r\n"
        "{file_content}\r\n"
        "--{boundary}--"
    ).format(
        boundary=boundary,
        field_name=field_name,
        filename=file_pointer.name,
        content_type=get_content_type(file_pointer.name),
        file_content=file_pointer.read()
    )

    print(body)

    content_type = f"multipart/form-data; boundary={boundary}"

    return body.encode('utf-8'), content_type


def post_file(
        host: str, path: str, field: str, file_pointer: TextIO
) -> HTTPResponse:
    body, content_type = file_to_body(field, file_pointer)
    h = client.HTTPConnection(host)
    h.putrequest('POST', path)
    h.putheader('content-type', content_type)
    h.putheader('content-length', str(len(body)))
    h.endheaders()
    h.send(body)
    response = h.getresponse()
    return response


def get_content_type(filename):
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'
