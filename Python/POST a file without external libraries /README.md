### Description
This example demonstrates how to send a file via POST request, as a `multipart/form-data` content type without using any external libraries. Tested on Python 3.7

# Example usage
```python
with open('some-file.csv') as f:
    response = post_file('example-host.com', '/path/to/endpoint', 'form-field-name', f)
```