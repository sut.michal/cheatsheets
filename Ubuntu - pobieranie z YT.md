# Pobieranie z YT na Ubuntu przy użyciu yt-dlp

Najwygodniej i najszybciej jest pobierać z YT przy pomocy yt-dlp. 
Potrzebne są:
1. Python 3.9+
2. Biblioteki `ffmpeg` i `ffprobe`, żeby pobierać w lepszej jakości:
    - `python3 -m pip install python-ffmpeg`
    - `python3 -m pip install ffprobe`
3. Plik [yt-dlp](https://github.com/yt-dlp/yt-dlp) (do pobrania z tego repo wersja na dzień 21.01.2025: https://gitlab.com/sut.michal/cheatsheets/-/blob/master/yt-dlp)
4. Uruchamiamy polecenie:
    - `python3 yt-dlp --cookies-from-browser firefox [YT_LINK]` (można użyć innej przegladarki, jeśli jest się zalogowanym na innej)
