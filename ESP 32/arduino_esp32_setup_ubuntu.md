# Setting up Arduino IDE + ESP32 library on a new computer (Ubuntu)

1. Download and install Arduino IDE [from here](https://www.arduino.cc/en/software)
1. Prepare your OS:
    1. Make sure Python is accessible under `python` name. If it's not (it is `python3` or something different), you can create a symbolic link, like this ([original link](https://stackoverflow.com/questions/60762378/exec-python-executable-file-not-found-in-path)): 
        ```shell
        sudo ln -s /usr/bin/python3 /usr/bin/python
        ```

    1. Install `pyserial`
        ```shell
        pip install pyserial
        ```
    
    1. Make sure your user can access USB port. Run those two commands ([original link](https://github.com/esp8266/source-code-examples/issues/26)):
        ```shell
        sudo usermod -a -G tty <yourname>
        ```

        ```shell
        cd /
        cd dev
        chown username ttyUSB0
        ```
        _(replace ttyUSB0 with correct name of your usb port)_
1. Install ESP32 Add-on ([original link](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/)):
    1. In your Arduino IDE, go to **File**> **Preferences**
    1. Enter `https://dl.espressif.com/dl/package_esp32_index.json` into the “Additional Board Manager URLs” field as shown in the figure below. Then, click the “OK” button
    1. Open the Boards Manager. Go to **Tools** > **Board** > **Boards Manager…**
    1. Search for ESP32 and press install button for the “ESP32 by Espressif Systems“:
    1. That’s it. It should be installed after a few seconds.
1. Test the installation
    1. Select your Board in **Tools** > **Board menu** (in my case it’s the **ESP32 Dev Module**)
    1. Connect USB cable to USB Port
    1. Select the Port (**Tools** -> **Ports**)
    1. Open the following example under **File** > **Examples** > **WiFi (ESP32)** > **WiFiScan** [It can be any other example]
    1. Press the Upload button in the Arduino IDE. Wait a few seconds while the code compiles and uploads to your board.
    1. If everything went as expected, you should see a “Done uploading.” message.
    1. Open the Arduino IDE Serial Monitor at a baud rate of 115200
    1. You should see the message with scanned wifi access points
