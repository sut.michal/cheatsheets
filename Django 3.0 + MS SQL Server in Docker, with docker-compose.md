### Django 3.0 + MS SQL Server in Docker, with docker-compose
1. MS SQL Server backend: https://pypi.org/project/django-mssql-backend/
2. Requirements
    ```python
    Django==3.0.6
    pyodbc==4.0.30
    django-mssql-backend==2.8.1
    ```
2. Dockerfile:

    ```docker
    FROM python:3.7
    ENV PYTHONUNBUFFERED 1
    
    # For ODBC driver
    RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
    RUN curl https://packages.microsoft.com/config/ubuntu/19.10/prod.list > /etc/apt/sources.list.d/mssql-release.list
    
    RUN apt-get update
    RUN apt-get install -y gettext
    
    # Install ODBC driver
    RUN export ACCEPT_EULA=Y && \
        apt-get install -y msodbcsql17 && \
        apt-get install -y mssql-tools && \
        echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> /root/.bash_profile && \
        echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> /root/.bashrc
    
    RUN mkdir /code
    WORKDIR /code
    ADD requirements.txt /code/
    RUN apt-get install -y unixodbc-dev && pip install -r requirements.txt
    
    ADD . /code/
    ```
3. docker-compose

    ```yml
    version: '3'
    
    services:
    
      db_mssql:
        image: mcr.microsoft.com/mssql/server:2019-latest
        environment:
          SA_PASSWORD: "SecretPassword@123"
          ACCEPT_EULA: "Y"
        volumes:
          - mssql-volume:/var/opt/mssql
        ports:
          - "1433:1433"
      django:
        build:
          context: .
          dockerfile: compose/Dockerfile-dev
        command: python3 manage.py runserver 0.0.0.0:8000
        volumes:
          - .:/code
        ports:
          - "8000:8000"
        depends_on:
          - db_mssql
    
    volumes:
      mssql-volume:
    ```

4. Django DATABASES config:

    ```python
    DATABASES = {
        'default': {
            'ENGINE': 'sql_server.pyodbc',
            'HOST': 'db_mssql',
            'NAME': 'offers',
            'USER': 'SA',
            'PASSWORD': 'SecretPassword@123',
            'OPTIONS': {
                'driver': 'ODBC Driver 17 for SQL Server',
            },
            'ATOMIC_REQUESTS': True
        }
    }
    ```