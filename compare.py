"""The script compares two different files with requirements and generates a comparison table.

The script looks at both requirements files (treating the second file as the newer) and detects which packages were
added, which were removed and which changed their version. Results are displayed in either CSV or Markdown table.

By default, results are printed to stdout in CSV format:

    python compare.py requirements-old.txt requirements-new.txt

Run python compare.py --help for more details on how to use it.
"""
__author__ = "Michał Sut"
__author_email__ = "sut.michal@gmail.com"
__copyright__ = "Copyright (C) 2023 Michał Sut"
__license__ = "GPLv3"
__version__ = "1.0"


import argparse
import re
import sys
from itertools import groupby

CSV = "csv"
MD = "md"
FORMATS = [CSV, MD]
HEADER = ["Previous", "New", "Change"]
CSV_SEP = "\t"


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("file1", type=argparse.FileType("r"), help="First requirements file")
    parser.add_argument("file2", type=argparse.FileType("r"), help="Second requirements file")
    parser.add_argument(
        "-o",
        "--out",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="Optional output file. Result is printed to stdout if not specified.",
    )
    parser.add_argument("-f", "--format", choices=FORMATS, default=CSV, help="Output format: CSV or Markdown table.")
    parser.add_argument("-s", "--csv-separator", default=CSV_SEP, help="CSV Delimiter. Used only if format is CSV.")
    parser.add_argument("-d", "--diff-only", action="store_true", help="Print only changed packages.")
    return parser.parse_args()


def load_requirements(file):
    """Get normalized list of requirements: without comments, sorted and all lowercase."""
    with file:
        req_lines = sorted(
            filter(lambda line: line and not line.startswith("#"), map(lambda s: s.strip().lower(), file.readlines()))
        )
        return req_lines


def get_name(req_line):
    """Get normalized package name from requirement line: [name][Version specifier][version].

    Example: Django>=4.2,<4.3 --> Django
    """
    parts = re.split("[=<>~^]", req_line, maxsplit=1)
    return re.sub(r"\[\w+]", "", parts[0].lower()).replace(".", "-")


def detect_change(requirement_1, requirement_2):
    if requirement_1 == requirement_2:
        return ""
    if not requirement_1:
        return "ADDED"
    if not requirement_2:
        return "REMOVED"
    return "VERSION CHANGED"


def build_comparison_table(requirements_1, requirements_2, diff_only=False):
    """
    Build a table with package requirements (old and new) in the same row together with information what has changed.

    The table is a list of lists (rows) sorted by requirement lines.
    """
    common_requirements = set(requirements_1).intersection(set(requirements_2))
    differing_requirements = set(requirements_1 + requirements_2).difference(common_requirements)

    if diff_only:
        comparison_table = []
    else:
        comparison_table = [[req, req, ""] for req in sorted(common_requirements)]

    # Group differing requirements by package name to compare their version.
    groups = groupby(sorted(differing_requirements), key=get_name)
    for name, group in groups:
        row = ["", "", ""]
        for req in group:
            if req in requirements_1:
                row[0] = req
            if req in requirements_2:
                row[1] = req
        comparison_table.append(row)

    for row in comparison_table:
        row[2] = detect_change(row[0], row[1])

    return sorted(comparison_table, key=lambda r: "".join(map(str, r)))


def generate_csv(comparison_table, out_file, sep=CSV_SEP):
    """Write comparison table to the output as a CSV table."""
    header = sep.join(HEADER) + "\n"
    out_file.write(header)
    for row in comparison_table:
        row_line = sep.join(row) + "\n"
        out_file.write(row_line)


def generate_markdown(comparison_table, out_file):
    """Write comparison table to the output as a Markdown table."""
    max_cell_length = 0
    for row in comparison_table + HEADER:
        max_cell_length = max(max_cell_length, *[len(cell) for cell in row])

    max_cell_length += 4  # Extra space for **emphasis**

    def md_row(cells, emphasis=False):
        if emphasis:
            cells = [f"**{cell}**" if cell else cell for cell in cells]
        return "|" + "|".join([f"{cell:<{max_cell_length}}" for cell in cells]) + "|"

    header = md_row(HEADER)
    line = md_row(["-" * max_cell_length for _ in HEADER])
    out_file.write(header + "\n" + line + "\n")

    for row in comparison_table:
        row_line = md_row(row, emphasis=bool(row[2])) + "\n"
        out_file.write(row_line)


def main():
    args = get_args()
    requirements_1 = load_requirements(args.file1)
    requirements_2 = load_requirements(args.file2)

    comparison_table = build_comparison_table(requirements_1, requirements_2, diff_only=args.diff_only)

    if args.format == CSV:
        generate_csv(comparison_table, args.out, sep=args.csv_separator)
    elif args.format == MD:
        generate_markdown(comparison_table, args.out)

    args.out.close()


if __name__ == "__main__":
    main()
