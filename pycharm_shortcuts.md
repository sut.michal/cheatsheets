SUPER = Window
Skróty klawiszowe:

## OS

### Shortcuts
- Click and press SUPER - look at your shortcuts
- Editor
-- Go(jump) by whole word, not single sign: CTRL + <-/-> 
-- Select all/everything: CTRL+A

- Reverse of some action: use SHIFT
-- Scrolling too wide file, tree project (pycharm)
-- Reverse switching CTRL+TAB
-- Reverse switching ALT+TAB 
- Window Management
-- Switch between opened app: ALT+TAB
-- Switch between window from the same app: ALT + `
-- Go to app nr. X from left bar: SUPER+X (press to get know what I mean)
-- Open terminal: CTRL+ALT+T
-- Switch between workspaces (if enabled) - CTRL + ALT + Arrows
-- Move app between workspaces (if enabled) - CTRL + ALT + SHIFT + Arrows
-- Maximize windows to full screen - CTRL+SUPER+ArrowUp
- Divide current windows into parts
-- half and place it left/right - CTRL+SUPER+Arrow Left/Right 
-- quarter, half itd... CTRL+ALT+Num_numbers
-- third party apps
- Prints:
-- Printscreen of current window: ALT + PRINT
-- CTRL+SHIFT+PRINT - take printscreen of part of screen and copy to cache

### Aliases
To type less and get the same results create some aliases, it is so easy:

1. Redirect making aliases in new file ~/.bash_aliases by adding following code to ~/.bashrc
```
if [-f ~/.bash_aliases ]; 
then . ~/.bash_aliases 
fi
```
2. Open ~/.bash_aliases
3. Write aliass that you want to make it shorter. For example:
alias <new_alias>='<command>'
alias gits='git status'
Save and exit.
4. Reload aliases by typing . ~/.basrc
5. Now you can use aliases


## Terminator
- Split vertically: CTRL+SHIFT+E
- Split horizontally: CTRL+SHIFT+O
- If you have got some splitted window then you can switching beetwen this: ALT + Arrows
- Jump to make windows cleared: CTRL+L
- Resize windows/tab: CTRL + SHIFT + Arrows
- 

## Chrome
- Go to URL placeholder: CTRL + L
- Go to 1st, 2nd, 3rd...8th tab - CTRL + <1...8>
- Go last tab CTRL+9
- Go previous/next tab from current - CTRL + PageUp/PageDown
- Move current tab left/right - CTRL + SHIFT + PageUp/PageDown
- Pinning cards

## PyCharm

### Shortcuts 
Everyone have various shortcuts (or default are not setted) but these funtions are good to be shortcutted:
- Python Console
- File structure windows 
- Extend/Shrink Selection
- Select in Project View
- Restore closed tab 
- Run/Debug/Stop
- Back, Forward 
- Debugger option while intensive debugging

### Shortcuts(should be default): 
- Autocompletion, Autofilling, hinting: Ctrl + Space
- Duplicate some part of code (default duplicate line if nothing is selected): CTRL + D 
- Copy,Cut line(delete) at cursor position: CTRL C/X
- Go to terminal to root of project: Alt + F12
- Go to declaration of class, method, variable: CTRL + LeftMouseClick, CTRL+B for mouse haters
- Search everywhere through code with a lot of searching option (Find in path): CTRL + SHIFT + F
- Search everywhere through methods, classes, files, path of file(Search everywhere): Double Shift
- Rename wisely file, method, class (Pycharm is looking for dependencies): Shift + F6
- Hide all tool windows: CTRL+SHIFT + F12
- Generate methods that can be overriden: CTRL + O 


### Debugger Shortcuts:
- One step over: F8
- Step to next breakpoint: F9
- Step into: F7
- Step out: Shift + 8 (missleading)
- Evaluate expression: 
- Watches: New: Insert
- Watches: Delete: Del
- Watches change watches: F2
- All breakpoints: CTRL+SHIFT+F8


### Fancy shortcuts:
- Column selection mode: Alt + Insert + 8, Alt + Left mouse press 
- Find and select next occurence of given phrase in current file: Alt+J
- Shift line/block of code: SHIFT + ALT + Arrow Down/Up
- Shift method above/below class,method: CTRL+SHIFT + Arrow Down/Up
- Hierarchy: Ctrl + H, Ctrl + Alt + H
- Split Vertically/ Split Horizontally
- Move cursor to next/previous windows (Goto next/previous splitter)
- Folding code: CTRL (alt, shift) + '-'/'+' 

### Maybe you haven't heard before
- If you often use some of tool (docker, vagrant, deployment itd) you can also bind actions
- You can define some initial code before you start python console(import os, import sys, some often used method, etc...)
- You have got all git features in PyCharm (resolving conflicts, show branch tree)
- TODOs
- VCS (conflict resolving)