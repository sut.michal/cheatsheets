# Mapping ESP32 pins to concrete signals to LED Cube


| Description     | Signal | Pin name | Pin Number | Notes                           |
|-----------------|--------|----------|------------|---------------------------------|
| Serial data     | IN     | TX0      | 1          |                                 |
| Reset?          | H      | D22      | 22         | Must be HIGH to make cube work. |
| Latch           | L      | RX0      | 3          |                                 |
| Clock           | C      | D21      | 21         |                                 |
| Ground (enable) | G      | D23      | 23         | Must be LOW to make cube work.  |
| Row A (bottom)  | A      | TX2      | 17         |                                 |
| Row B           | B      | D5       | 5          |                                 |
| Row C           | C      | D18      | 18         |                                 |
| Row D (top)     | D      | D19      | 19         |                                 |
