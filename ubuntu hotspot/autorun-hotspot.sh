iteration=1;
while [ $iteration -le 5 ] ; do
        string=`ethtool ETHERNET_DEVICE_NAME 2> /dev/null | grep "Link detected"`;
        if [[ $string == *"Link detected: yes"* ]]; then
                echo "It's there!";
                sleep 0.5;
                . run-hotspot.sh;
                break;
        elif [[ $string == *"Link detected: no"* ]]; then
                echo "Wait a bit";
                sleep 1;
        else
                sleep 1;
        fi
        let "iteration+=1";
done
