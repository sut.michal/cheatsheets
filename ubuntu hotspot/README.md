### Ubuntu auto-enable wifi hotspot
Those scripts allow you to automatically start WiFi hotspot on startup if ethernet is connected.

This script was written and tested on **Ubuntu 19.10** and is not guaranteed it will work on other versions.

#### Prerequisites:

1. You need `ethtool` program installed:
```
sudo apt install ethtool
```

#### Instructions:

1. In `autorun-hotspot.sh` file, replace `ETHERNET_DEVICE_NAME` with an actual name of your Ethernet interface. You can find network interfaces in several different ways, for example:
 * `ls /sys/class/net` will list only names of interfaces
 * `ip link` will show more details for each interface

 You need Ethernet interface, so it's name usually starts with `eth` or `enp` or `enx` or something similar.
1. In `run-hotspot.sh` file, set your hotspot SSID and password replacing respectively: `this_is_ssid` and `this_is_password` strings. 
1. All 3 scripts should be added to `$PATH`. You can create a `bin` directory in `$HOME`, so all scripts inside that directory will be automatically in `$PATH`.
1. Add a script to startup. Go to "Startup application" in Ubuntu (builtin GUI program of Ubuntu). Click "Add" and browse for your script file `autorun-hotspot.sh`


#### Usage:
Hotspot should start automatically on system startup. If you want to stop it, run `. stop-hotspot.sh`. If you want to start it manually, run `. run-hotspot.sh`

