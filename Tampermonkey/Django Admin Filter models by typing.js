// ==UserScript==
// @name         Django Admin Filter models by typing
// @namespace    http://tampermonkey.net/
// @version      2024-05-22
// @description  try to take over the world!
// @author       You
// @match        https://schloud.szkolawchmurze.org/admin*
// @match        https://schloud-beta.stg-k8s.szkolawchmurze.org/admin*
// @match        https://api-beta.stg-k8s.szkolawchmurze.org/admin*
// @match        http://localhost:8001/admin/
// @match        http://localhost:8001/admin/*
// @match        http://localhost:8000/admin/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=szkolawchmurze.org
// @grant        none
// ==/UserScript==

function debounce(cb, interval, immediate) {
  var timeout;

  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) cb.apply(context, args);
    };

    var callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, interval);

    if (callNow) cb.apply(context, args);
  };
};


function getHighlightStyle(){
    const currentTheme = localStorage.getItem("theme") || "auto";
    const prefersDark = window.matchMedia("(prefers-color-scheme: dark)").matches;
    let theme = currentTheme;

    if (prefersDark) {
                // Auto (dark) -> Light -> Dark
        if (currentTheme === "auto") {
            theme = "dark";
        } else if (currentTheme === "light") {
            theme = "light";
        } else {
            theme = "dark";
        }
    } else {
        // Auto (light) -> Dark -> Light
        if (currentTheme === "auto") {
            theme = "light";
        } else if (currentTheme === "dark") {
            theme = "dark";
        } else {
            theme = "light";
        }
    }

    return theme === "light" ? "highlight-light" : "highlight-dark";
}


function highlight(text) {
  const color = getHighlightStyle();
  console.log(color);
  const rows = document.querySelectorAll("tr[class^='model-']:not(.is-hidden-for-search) th[scope='row'] a");
  rows.forEach(function(row){
      const rowText = row.innerText;
      const index = rowText.toLowerCase().indexOf(text.toLowerCase());

      const newRowText = rowText.substring(0, index) + "<span class=\"highlight " + color + "\">" + rowText.substring(index, index+text.length) + "</span>" + rowText.substring(index+text.length);
      row.innerHTML = newRowText;
  });
}


function updateHighlightColor(){
    const spans = document.querySelectorAll("span.highlight");
    const currentTheme = localStorage.getItem("theme") || "auto";
    const prefersDark = window.matchMedia("(prefers-color-scheme: dark)").matches;

    spans.forEach(function(span){
        if (prefersDark) {
            // Auto (dark) -> Light -> Dark
            if (currentTheme === "auto") {
                span.classList.remove("highlight-light");
                span.classList.add("highlight-dark");
            } else if (currentTheme === "light") {
                span.classList.remove("highlight-dark");
                span.classList.add("highlight-light");
            } else {
                span.classList.remove("highlight-light");
                span.classList.add("highlight-dark");
            }
        } else {
            // Auto (light) -> Dark -> Light
            if (currentTheme === "auto") {
                span.classList.remove("highlight-dark");
                span.classList.add("highlight-light");
            } else if (currentTheme === "dark") {
                span.classList.remove("highlight-light");
                span.classList.add("highlight-dark");
            } else {
                span.classList.remove("highlight-dark");
                span.classList.add("highlight-light");
            }
        }
    })
}

function update_app_divs(){
    const appDivs = document.querySelectorAll("div[class^='app-']");
    appDivs.forEach(function(appDiv){
        const stillVisible = appDiv.querySelectorAll("tr:not(.is-hidden-for-search)");
        if (stillVisible.length == 0){appDiv.style.display='none'} else {appDiv.style.display=''}
    })
}


(function() {
    'use strict';
    const css = `
    <style>
    .highlight-dark {
        background-color: #045e01;
    }
    .highlight-light {
        background-color: yellow
    }
    </style>
    `;

    const allRows = document.querySelectorAll("tr[class^='model-']");
    document.head.innerHTML += css;


    let input = document.createElement("input");
    input.classList.add("vTextField");
    input.style.margin = '0 0 20px';
    input.placeholder = "Type to search models";
    input.id = "model-search-input";
    if (location.pathname === "/admin/"){
        document.getElementById("content").getElementsByTagName("h1")[0].insertAdjacentElement("afterend", input);
    } else {
        input.style.margin = '5px 0 5px';
        document.getElementById("nav-sidebar").insertAdjacentElement("afterbegin", input);
        document.getElementById("nav-filter").style = "display: none";
    }
    input.focus();

    input.addEventListener('keyup', debounce(function(event){
        allRows.forEach(function(row){
            if (!row.querySelector("th").innerText.toLowerCase().includes(event.target.value.toLowerCase())) {
                row.style.display = 'none';
                row.classList.add('is-hidden-for-search');
            } else {
                row.style.display = '';
                row.classList.remove('is-hidden-for-search');
            }
        });
        update_app_divs();
        highlight(event.target.value);
    }, 50))

    const buttons = document.getElementsByClassName("theme-toggle");
    Array.from(buttons).forEach((btn) => {
        btn.addEventListener("click", updateHighlightColor);
    });


    document.addEventListener('keydown', function(e) {
        if (!document.getElementById("model-search-input").value){
            return;
        }
        const rows = document.querySelectorAll("tr[class^='model-']:not(.is-hidden-for-search) th[scope='row'] a");
        let activeRowIdx = -1;
        if (e.key === "ArrowDown" || e.key === "ArrowUp"){
            // looking for current active element
            for (let i = 0; i < rows.length; i++){
                if (document.activeElement === rows[i]){
                    activeRowIdx = i;
                    break;
                }
            }
        }
        if (e.key === "ArrowDown") {
            rows[Math.min(activeRowIdx+1, rows.length-1)].focus();
            e.preventDefault();
        } else if (e.key === "ArrowUp") {
            rows[Math.max(activeRowIdx-1, 0)].focus();
            e.preventDefault();
        }
    });
})();