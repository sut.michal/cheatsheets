urls_addon = """# AUTO-ADDED-DEBUG-TOOLBAR

import debug_toolbar
import re
from django.urls import re_path
urlpatterns += [
    re_path(
        r"^%s/" % re.escape("__debug__".lstrip("/")),
        include("debug_toolbar.urls")
    )
]
"""

settings_addon = """# AUTO-ADDED-DEBUG-TOOLBAR

INSTALLED_APPS += ["debug_toolbar"]
MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda request: True,
}

"""

utils_addon = """# AUTO-ADDED-DEBUG-UTILS

from contextlib import contextmanager
@contextmanager
def log_sql(file_prefix=""):
    from django.db import connection
    from django.test.utils import CaptureQueriesContext
    import datetime
    with CaptureQueriesContext(connection) as queries:
        try:
            yield queries
        finally:
            lines = []
            with open(file_prefix + datetime.datetime.now().isoformat() + ".sql.txt", mode="w") as f:
                for query in queries.captured_queries:
                    lines.append(query["sql"] + "\\n")
                f.writelines(lines)


def save_explain(queryset, file_prefix=""):
    import datetime
    with open("EXPLAIN_" + file_prefix + datetime.datetime.now().isoformat() + ".sql.txt", mode="w") as f:
        f.write(queryset.explain(analyze=True, buffers=True))


from rest_framework.permissions import AllowAny
from users.models import SchloudUser
class AllowAnyId(AllowAny):
    def has_permission(self, request, view):
        request.user = SchloudUser.objects.get(id=view.kwargs["user_id"])
        request.user.user_id = request.user.id
        return True

"""


base_dir = "/home/michalsut/Projects/schloud/"


with open(base_dir + "schloud_api/settings.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-TOOLBAR" not in text:
        text = text + settings_addon

with open(base_dir + "schloud_api/settings.py", "w") as f:
    f.write(text)


with open(base_dir + "schloud_api/urls.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-TOOLBAR" not in text:
        text = text + urls_addon

with open(base_dir + "schloud_api/urls.py", "w") as f:
    f.write(text)
    
    
with open(base_dir + "utils/utils.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-UTILS" not in text:
        text = text + utils_addon

with open(base_dir + "utils/utils.py", "w") as f:
    f.write(text)

