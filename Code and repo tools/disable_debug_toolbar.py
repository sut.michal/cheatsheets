base_dir = "/home/michalsut/Projects/schloud/"


with open(base_dir + "schloud_api/settings.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-TOOLBAR" in text:
        text = text[:text.index("# AUTO-ADDED-DEBUG-TOOLBAR")]
        text = text.strip() + "\n"

with open(base_dir + "schloud_api/settings.py", "w") as f:
    f.write(text)


with open(base_dir + "schloud_api/urls.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-TOOLBAR" in text:
        text = text[:text.index("# AUTO-ADDED-DEBUG-TOOLBAR")]
        text = text.strip() + "\n"

with open(base_dir + "schloud_api/urls.py", "w") as f:
    f.write(text)
    
    
with open(base_dir + "utils/utils.py", "r") as f:
    text = f.read()
    if "# AUTO-ADDED-DEBUG-UTILS" in text:
        text = text[:text.index("# AUTO-ADDED-DEBUG-UTILS")]
        text = text.strip() + "\n"

with open(base_dir + "utils/utils.py", "w") as f:
    f.write(text)
