#!/usr/bin/bash

(cd /home/michalsut/Projects/schloud && git for-each-ref --format='%(authorname) %(refname)' --sort=authorname | grep michalsut | grep 'remotes/origin' --color=never | sed 's/^michalsut\ refs\/remotes\/origin\//https:\/\/git\.szkolawchmurze\.org\/platforma\/schloud\/-\/branches\?state=all\&sort=updated_desc\&search=/' | sed 's/\(.*\)/<a href=\"\1\">\1<\/a><br>/');

