import subprocess
import pathlib
import sys
import os


if len(sys.argv) > 1:
    project_name = sys.argv[1]
else:
    project_name = "schloud"

BASE = f"/home/michalsut/Projects/{project_name}"

git_script = f"""#!/usr/bin/bash

(cd {BASE} && git $@);
"""

with open("git", "w") as f:
    f.write(git_script)
os.chmod("git", 0o777)


files = subprocess.check_output(["./git", "diff", "--cached", "--name-only"], universal_newlines=True)
files = files.strip().split("\n")

files2 = subprocess.check_output(["./git", "diff", "--name-only"], universal_newlines=True)
files2 = files2.strip().split("\n")

files = files + files2

full_names = []
for file_ in files:
    if file_ and file_.endswith(".py"):
        full_names.append(pathlib.Path(BASE) / pathlib.Path(file_))

if not full_names:
    print("No changes detected")
    sys.exit()

print("Isort:")
subprocess.run(["isort", *full_names, "--src-path", BASE, "--profile", "black"])

print("\nBlack:")
subprocess.run(["black", *full_names])

print("\nRuff:")
subprocess.run(["ruff", *full_names])
