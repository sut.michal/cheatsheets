def use_raw_dicts_in_serializers(func_or_class):
    """
    Decorator that runs a test function in a mocked context, where `OrderDict` is mocked as a regular `dict`
    in `rest_framework.serializers` module. Thanks to that mocking, serialized data is not longer nested OrderDict,
    but a regular dict, so diff between expected and actual dicts are more readable.

    Decorator can be applied either on a single test method in a TestCase class or on TestCase class itself.

    Example usage:
    @use_raw_dicts_in_serializers
    class SimilarOfferApartmentTestCase(TestCase):
        ...

    or

    class SimilarOfferApartmentTestCase(TestCase):

        @use_raw_dicts_in_serializers
        def setUp(self):
            ...

        @use_raw_dicts_in_serializers
        def test_something(self):
            ...

    """
    if isclass(func_or_class):
        test_methods = getmembers(func_or_class, predicate=lambda m: isfunction(m) and m.__name__.startswith("test_"))
        test_methods += [("setUp", func_or_class.setUp)]
        for func_name, func_obj in test_methods:
            setattr(func_or_class, func_name, use_raw_dicts_in_serializers(func_obj))
        return func_or_class

    elif isfunction(func_or_class):

        @functools.wraps(func_or_class)
        def _wrapper(*args, **kwargs):
            with mock.patch("rest_framework.serializers.OrderedDict", dict):
                return func_or_class(*args, **kwargs)

        return _wrapper
