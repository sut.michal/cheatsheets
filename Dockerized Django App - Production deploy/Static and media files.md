# How to make static files work in VPS

:warning: This guide assumes an application runs behind Nginx proxy. There should be at least two containers: for app and for proxy.


### 1. Web app Dockerfile.
In a Dockerfile of webapp container, create directories defined in a previous step and add permisions to those directories to the user
```Dockerfile
# This is only a fragment of a Dockerfile

# Make directories for media and static files
RUN mkdir -p /var/www/media
RUN mkdir -p /var/www/static
# Create a user with less priviledge than root, for   safety. Set permission on static and media directories
RUN adduser -D user
RUN chown -R user:user /var/www
RUN chmod -R 755 /var/www

# Switch to user created
USER user
```

### 2. Proxy Dockerfile
In a Dockerfile for proxy (nginx) create directories for static and media files

```Dockerfile
# Temporarily switch to root in order to create directories
USER root

RUN mkdir -p /var/static
RUN mkdir -p /var/media
RUN chmod 755 /var/static
RUN chmod 755 /var/media

# Swtich back to unprivileged user
USER nginx

```

### 3. Docker Compose file
In a produiction Docker Compose file, create volumes for static and media directories shared between proxy and web app containers

```yaml
# This are only fragments of docker-compose.yml which 
# are relevant in context of static and media files 

services:
  django:
    volumes:
      - static_data:/var/www/static
      - media:/var/www/media
  proxy:
    volumes:
      - static_data:/var/static
      - media:/var/media

volumes:
  static_data:
  media:

```

### Proxy configuration
In a proxy config file, set location alias for static and media files

nginx.conf:
```conf
server {
    listen 8080;

    # This is for STATIC
    location /static {
        alias /var/static;
    }

    # This is for MEDIA
    location /media {
        alias /var/media;
    }

    location / {
        uwsgi_pass django:8000;
        include /etc/nginx/uwsgi_params;
    }
}
```

Make sure values for `alias` are the same as those for volumes in Compose file for proxy service

### Django settings
1. In Django production settings, set `STATIC_ROOT` and `MEDIA_ROOT` to directories where you want to store static files (collected by `collectstatic`) and media files (uploaded by files). A good example is setting it to something like this:
    ```python
    STATIC_ROOT = "/var/www/static"
    MEDIA_ROOT = "/var/www/media"
    ```

    These paths should be the same as path for volumes in Compose file of "django" service

2. Also, set URL prefix for static and media files
    ```python
    STATIC_URL = "/static/"
    MEDIA_URL = "/media/"
    ```

    These paths should be the same as locations defined in `nginx.conf`
