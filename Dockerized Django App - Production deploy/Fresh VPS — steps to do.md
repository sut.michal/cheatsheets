# Steps to do on a new, fresh VPS instance

1. Install `docker`:
    
    https://docs.docker.com/engine/install/ubuntu/ 

    * `sudo apt-get update`
    * `sudo apt-get install ca-certificates curl gnupg lsb-release`
    * `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`
    * `echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`
    * `sudo apt-get update` (_again_)
    * `sudo apt-get install docker-ce docker-ce-cli containerd.io`
    * `sudo docker run hello-world` — test if installed sucessfully

2. Install `docker-compose`:

    https://docs.docker.com/compose/install/

    * `sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
    * `sudo chmod +x /usr/local/bin/docker-compose`
    * `sudo curl -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose -o etc/bash_completion.d/docker-compose`
    * ` source ~/.bashrc`
    * `docker-compose --version`

3. Clone repository

    :warning: NOTE: Remote repository (GitLab) needs to have SSH Public Key of a requester registered. Instead of registering VPS's key, you can forward your own SSH Keys. To do this, while connecting to the server via SSH, add `-A` option:

    ```shell
    ssh -A <username>@<host>
    ```

    1. Git clone remote repository
        ```shell
        $ git clone <SSH link to repo>
        ```

4. Build containers

    1. Get in the project root directory (`cd <project_name`>)
    2. Create `.env` files and set production variables if necessary
    3. Build all containers:
        ```shell
        docker-compose -f docker-compose-prod.yml build
        ```

5. Run:
    ```shell
    docker-compose -f docker-compose-prod.yml up -d
    ```
